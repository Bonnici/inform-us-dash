# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from dash.dependencies import Input, Output

app = dash.Dash()

app.layout = html.Div([
    html.Div([
         html.Div([
            html.Div('Target Metrics', className = 'twelve columns row-title')
        ], className = 'row'),

        html.Div([
            html.Div([
                html.Div([html.Span([], className = 'medical-icon-i-anesthesia')], className = 'card-icon'),
                html.Div('84%', className = 'card-statistic'),
                html.H4('Tidal Volume Targets')
            ], className = 'four columns card', id = 'top-level-tv', n_clicks = 0),

            html.Div([
                html.Div([html.Span([], className = 'medical-icon-i-cardiology')], className = 'card-icon'),
                html.Div('99%', className = 'card-statistic'),
                html.H4('Blood Pressure Targets')
            ], className = 'four columns card', id = 'top-level-bp', n_clicks = 0),

            html.Div([
                html.Div([html.Span([], className = 'medical-icon-i-intensive-care')], className = 'card-icon'),
                html.Div('65%', className = 'card-statistic'),
                html.H4('Fluid balance')
            ], className = 'four columns card', id = 'top-level-fb', n_clicks = 0)
        ], className = 'row row-title h1'),
    ], className = 'row-shaded'),

    html.Div([
        html.Div(
            dcc.Graph(
                id='tv-graph',
                figure={
                    'data': [
                        {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'TV'},
                        {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': 'NY'},
                    ],
                    'layout': {
                        'title': 'Tidal Volume Targets'
                    }
                }
            ),
        className = 'nine.columns offset-by-three.columns')
    ], className = 'row', id = 'drill-down-tv'),

    html.Div([
        html.Div(
            dcc.Graph(
                id='bp-graph',
                figure={
                    'data': [
                        {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'BP'},
                        {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': 'NY'},
                    ],
                    'layout': {
                        'title': 'BP Targets'
                    }
                }
            ),
        className = 'nine.columns offset-by-three.columns')
    ], className = 'row', id = 'drill-down-bp'),

    html.Div([
        html.Div(
            dcc.Graph(
                id='fb-graph',
                figure={
                    'data': [
                        go.Bar(
                            x = ["1 Year", "3 Year", "5 Year", "10 Year", "41 Year"],
                            y = ["21.67", "11.26", "15.62", "8.37", "11.11"],
                            marker = {"color": "rgb(253, 235, 246)"},
                            name = "Fluid Balance"
                        ),
                        go.Bar(
                            x = ["1 Year", "3 Year", "5 Year", "10 Year", "41 Year"],
                            y = ["21.83", "11.41", "15.79", "8.50"],
                            marker = {"color": "rgb(149, 187, 199)"},
                            name = "Comparator"
                        ) 
                    ],
                    'layout': go.Layout(
                                bargap = 0.35,
                                font = {
                                  "family": "Gill Sans",
                                  "size": 16
                                },
                                title = "Fluid Balance Targets"
                    )
                }
            ),
        className = 'nine.columns offset-by-three.columns')
    ], className = 'row', id = 'drill-down-fb'),

], className = 'container')

# @app.callback(
#     Output('drill-down-tv', 'style'),
#     [Input('top-level-tv','n_clicks')]
# )

# @app.callback(
#     Output('drill-down-bp', 'style'),
#     [Input('top-level-bp','n_clicks')]
# )

@app.callback(
    Output('drill-down-fb', 'style'),
    [Input('top-level-fb','n_clicks')]
)

def toggle_graph(n_clicks):
    if n_clicks % 2 == 1:
        return {
            'transition': 'max-height 1s ease-out',
            'max-height': '600px'
        }
    else:
        return {
            'transition': 'max-height 1s ease-out',
            'max-height': '0'
        }

if __name__ == '__main__':
    app.run_server(debug=True)